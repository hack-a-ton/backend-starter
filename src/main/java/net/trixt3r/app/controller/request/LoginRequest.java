package net.trixt3r.app.controller.request;

/**
 * Created by cristi on 22.04.2017.
 */
public class LoginRequest {
	String username;
	String password;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
