package net.trixt3r.app.controller.response;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by cristi on 22.04.2017.
 */
public class LoginResponse {
	@JsonInclude(JsonInclude.Include.NON_NULL)
	String token;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	String error;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}
}
