package net.trixt3r.app.controller;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import net.trixt3r.app.repo.entity.Test;
import net.trixt3r.app.repo.TestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by cristi on 20.04.2017.
 */
@RestController
@RequestMapping("/api")
public class DemoController {

  @Autowired
  TestRepository repository;

  @RequestMapping(value = "/greeting", method = RequestMethod.GET)
  public ResponseEntity<?> greeting() {
    return ResponseEntity.ok("Hello!");
  }

  @RequestMapping(value = "/values", method = RequestMethod.GET)
  public ResponseEntity<?> values() {
    List<Test> result =
        StreamSupport
            .stream(repository.findAll().spliterator(), false)
            .collect(Collectors.toList());

    return ResponseEntity.ok(result);
  }
}
