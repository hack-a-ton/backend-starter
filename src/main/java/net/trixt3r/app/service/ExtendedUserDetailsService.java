package net.trixt3r.app.service;

import java.util.HashSet;
import java.util.Set;

import net.trixt3r.app.model.ExtendedUserDetail;
import net.trixt3r.app.repo.UserRepository;
import net.trixt3r.app.repo.entity.Role;
import net.trixt3r.app.repo.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * Created by cristi on 22.04.2017.
 */
@Service
public class ExtendedUserDetailsService
		implements org.springframework.security.core.userdetails.UserDetailsService {
	UserRepository userRepository;

	public ExtendedUserDetailsService(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException {

		User user = userRepository.findByUsername(username);

		ExtendedUserDetail userDetail =
				new ExtendedUserDetail(
						user.getUsername(),
						user.getPassword(),
						getAuthorities(user)
				);

		userDetail.setAvatar(user.getAvatarHref());
		userDetail.setFirstName(user.getFirstName());
		userDetail.setLastName(user.getLastName());

		return userDetail;
	}

	private Set<GrantedAuthority> getAuthorities(User user){
		Set<GrantedAuthority> authorities = new HashSet<>();
		for(Role role : user.getRoles()) {
			GrantedAuthority grantedAuthority = new SimpleGrantedAuthority(role.getRole());
			authorities.add(grantedAuthority);
		}
		return authorities;
	}
}