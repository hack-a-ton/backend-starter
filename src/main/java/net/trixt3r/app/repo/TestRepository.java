package net.trixt3r.app.repo;

import net.trixt3r.app.repo.entity.Test;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by cristi on 21.04.2017.
 */

public interface TestRepository extends CrudRepository<Test, Integer> {

}