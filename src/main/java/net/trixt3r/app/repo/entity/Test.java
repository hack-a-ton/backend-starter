package net.trixt3r.app.repo.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Created by cristi on 21.04.2017.
 */
@Entity
public class Test {
	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	Integer id;
	String value;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
