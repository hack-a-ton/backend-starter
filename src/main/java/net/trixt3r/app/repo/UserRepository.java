package net.trixt3r.app.repo;

import net.trixt3r.app.repo.entity.User;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by cristi on 22.04.2017.
 */
public interface UserRepository extends CrudRepository<User, Integer> {

	User findByUsername(String username);
}
